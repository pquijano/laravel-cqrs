<?php

declare(strict_types=1);

namespace PQuijano\Tests\Unit\Buses;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Orchestra\Testbench\TestCase as AbstractTestCase;
use PHPUnit\Framework\Attributes\CoversClass;
use PQuijano\LaravelCQRS\Abstracts\Command as AbstractCommand;
use PQuijano\LaravelCQRS\Buses\CommandBus;
use PQuijano\LaravelCQRS\Exceptions\Buses\NotImplementedCommandException;
use PQuijano\LaravelCQRS\Exceptions\Buses\NotImplementedCommandHandlerException;
use PQuijano\LaravelCQRS\Interfaces\Handlers\CommandHandler as CommandHandlerInterface;
use ReflectionClass;

#[CoversClass(CommandBus::class)]
#[CoversClass(NotImplementedCommandException::class)]
#[CoversClass(NotImplementedCommandHandlerException::class)]
final class CommandBusTest extends AbstractTestCase
{
    public function test_returns_successful(): void
    {
        $commandBus = new CommandBus();
        $command = new class extends AbstractCommand
        {
        };
        App::shouldReceive('make')
            ->once()
            ->with($this->handlerName($command))
            ->andReturn(new class implements CommandHandlerInterface
            {
                public function handle($command)
                {
                    return true;
                }
            });
        $result = $commandBus->send($command);
        $this->assertTrue($result);
    }

    public function test_throws_not_implemented_command_handler_interface_exception(): void
    {
        $commandBus = new CommandBus();
        $command = new class extends AbstractCommand
        {
        };
        App::shouldReceive('make')
            ->once()
            ->with($this->handlerName($command))
            ->andReturn(new class
            {
            });
        $this->assertThrows(
            fn () => $commandBus->send($command),
            NotImplementedCommandHandlerException::class
        );
    }

    public function test_with_not_command_interface_parameter_throws_not_implemented_command_interface_exception(): void
    {
        $commandBus = new CommandBus();
        $command = new class
        {
        };
        $this->assertThrows(
            fn () => $commandBus->send($command),
            NotImplementedCommandException::class
        );
    }

    protected function handlerName($command): string
    {
        $reflection = new ReflectionClass($command);
        $handlerName = Str::replace('Command', 'CommandHandler', $reflection->getShortName());
        $handlerName = Str::replace($reflection->getShortName(), $handlerName, $reflection->getName());

        return Str::replace('Commands', 'Handlers\\Commands', $handlerName);
    }
}
