<?php

declare(strict_types=1);

namespace PQuijano\Tests\Unit\Buses;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Orchestra\Testbench\TestCase as AbstractTestCase;
use PHPUnit\Framework\Attributes\CoversClass;
use PQuijano\LaravelCQRS\Abstracts\Query as AbstractQuery;
use PQuijano\LaravelCQRS\Buses\QueryBus;
use PQuijano\LaravelCQRS\Exceptions\Buses\NotImplementedQueryException;
use PQuijano\LaravelCQRS\Exceptions\Buses\NotImplementedQueryHandlerException;
use PQuijano\LaravelCQRS\Interfaces\Handlers\QueryHandler as QueryHandlerInterface;
use ReflectionClass;

#[CoversClass(QueryBus::class)]
#[CoversClass(NotImplementedQueryException::class)]
#[CoversClass(NotImplementedQueryHandlerException::class)]
final class QueryBusTest extends AbstractTestCase
{
    public function test_returns_successful(): void
    {
        $queryBus = new QueryBus();
        $query = new class extends AbstractQuery
        {
        };
        App::shouldReceive('make')
            ->once()
            ->with($this->handlerName($query))
            ->andReturn(new class implements QueryHandlerInterface
            {
                public function handle($query)
                {
                    return true;
                }
            });
        $result = $queryBus->send($query);
        $this->assertTrue($result);
    }

    public function test_throws_not_implemented_query_handler_interface_exception(): void
    {
        $queryBus = new QueryBus();
        $query = new class extends AbstractQuery
        {
        };
        App::shouldReceive('make')
            ->once()
            ->with($this->handlerName($query))
            ->andReturn(new class
            {
            });
        $this->assertThrows(
            fn () => $queryBus->send($query),
            NotImplementedQueryHandlerException::class
        );
    }

    public function test_with_not_query_interface_parameter_throws_not_implemented_query_interface_exception(): void
    {
        $queryBus = new QueryBus();
        $query = new class
        {
        };
        $this->assertThrows(
            fn () => $queryBus->send($query),
            NotImplementedQueryException::class
        );
    }

    protected function handlerName($command): string
    {
        $reflection = new ReflectionClass($command);
        $handlerName = Str::replace('Query', 'QueryHandler', $reflection->getShortName());
        $handlerName = Str::replace($reflection->getShortName(), $handlerName, $reflection->getName());

        return Str::replace('Queries', 'Handlers\\Queries', $handlerName);
    }
}
