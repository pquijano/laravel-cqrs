# Laravel CQRS
A simple implementation of CQRS for Laravel.

## Installation
```shell
composer require pquijano/laravel-cqrs:^1.0
```

## Usage
1. Create Commmands, Queries and Handlers.
```php
<?php

declare(strict_types=1);

namespace App\Commands;

use PQuijano\LaravelCQRS\Abstracts\Command as AbstractCommand;

final class SampleCommand extends AbstractCommand
{
    //
}
```
```php
<?php

declare(strict_types=1);

namespace App\Handlers\Commands;

use PQuijano\LaravelCQRS\Interfaces\Handlers\CommandHandler as CommandHandlerInterface;

final class SampleCommandHandler implements CommandHandlerInterface
{
    /**
     * @param  \App\Commands\SampleCommand  $command
     * @return mixed
     */
    public function handle($command)
    {
        //
    }
}
```
```php
<?php

declare(strict_types=1);

namespace App\Queries;

use PQuijano\LaravelCQRS\Abstracts\Query as AbstractQuery;

final class SampleQuery extends AbstractQuery
{
    //
}
```
```php
<?php

declare(strict_types=1);

namespace App\Handlers\Queries;

use PQuijano\LaravelCQRS\Interfaces\Handlers\QueryHandler as QueryHandlerInterface;

final class SampleQueryHandler implements QueryHandlerInterface
{
    /**
     * @param  \App\Queries\SampleQuery  $query
     * @return mixed
     */
    public function handle($query)
    {
        //
    }
}
```
2. Execute handlers using the CommandBus/QueryBus Facade.
```php
<?php

declare(strict_types=1);

use App\Commands\SampleCommand;
use App\Queries\SampleQuery;
use PQuijano\LaravelCQRS\Facades\CommandBus;
use PQuijano\LaravelCQRS\Facades\QueryBus;

CommandBus::send(new SampleCommand());
QueryBus::send(new SampleQuery());
```
