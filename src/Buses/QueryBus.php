<?php

declare(strict_types=1);

namespace PQuijano\LaravelCQRS\Buses;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use PQuijano\LaravelCQRS\Exceptions\Buses\NotImplementedQueryException;
use PQuijano\LaravelCQRS\Exceptions\Buses\NotImplementedQueryHandlerException;
use PQuijano\LaravelCQRS\Interfaces\Buses\QueryBus as QueryBusInterface;
use ReflectionClass;

final class QueryBus implements QueryBusInterface
{
    public function send($query)
    {
        if (! $query instanceof \PQuijano\LaravelCQRS\Abstracts\Query) {
            throw new NotImplementedQueryException($query);
        }
        $handler = App::make($this->handlerName($query));
        if (! $handler instanceof \PQuijano\LaravelCQRS\Interfaces\Handlers\QueryHandler) {
            throw new NotImplementedQueryHandlerException($handler);
        }

        return $handler->handle($query);
    }

    protected function handlerName($command): string
    {
        $reflection = new ReflectionClass($command);
        $handlerName = Str::replace('Query', 'QueryHandler', $reflection->getShortName());
        $handlerName = Str::replace($reflection->getShortName(), $handlerName, $reflection->getName());

        return Str::replace('Queries', 'Handlers\\Queries', $handlerName);
    }
}
