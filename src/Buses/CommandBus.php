<?php

declare(strict_types=1);

namespace PQuijano\LaravelCQRS\Buses;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use PQuijano\LaravelCQRS\Exceptions\Buses\NotImplementedCommandException;
use PQuijano\LaravelCQRS\Exceptions\Buses\NotImplementedCommandHandlerException;
use PQuijano\LaravelCQRS\Interfaces\Buses\CommandBus as CommandBusInterface;
use ReflectionClass;

final class CommandBus implements CommandBusInterface
{
    public function send($command)
    {
        if (! $command instanceof \PQuijano\LaravelCQRS\Abstracts\Command) {
            throw new NotImplementedCommandException($command);
        }
        $handler = App::make($this->handlerName($command));
        if (! $handler instanceof \PQuijano\LaravelCQRS\Interfaces\Handlers\CommandHandler) {
            throw new NotImplementedCommandHandlerException($handler);
        }

        return $handler->handle($command);
    }

    protected function handlerName($command): string
    {
        $reflection = new ReflectionClass($command);
        $handlerName = Str::replace('Command', 'CommandHandler', $reflection->getShortName());
        $handlerName = Str::replace($reflection->getShortName(), $handlerName, $reflection->getName());

        return Str::replace('Commands', 'Handlers\\Commands', $handlerName);
    }
}
