<?php

declare(strict_types=1);

namespace PQuijano\LaravelCQRS\Exceptions\Buses;

use Exception;
use Throwable;

final class NotImplementedCommandException extends Exception
{
    public function __construct(object $object, $code = 0, ?Throwable $previous = null)
    {
        $message = 'The object \''.get_class($object).'\' does not implement \''.\PQuijano\LaravelCQRS\Interfaces\Command::class.'\'.';
        parent::__construct($message, $code, $previous);
    }
}
