<?php

declare(strict_types=1);

namespace PQuijano\LaravelCQRS\Interfaces\Buses;

/**
 * @template TQuery of \PQuijano\LaravelCQRS\Abstracts\Query
 * @template TReturn
 */
interface QueryBus
{
    /**
     * @param  TQuery  $query
     * @return TReturn
     */
    public function send($query);
}
