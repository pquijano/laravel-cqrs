<?php

declare(strict_types=1);

namespace PQuijano\LaravelCQRS\Interfaces\Buses;

/**
 * @template TCommand of \PQuijano\LaravelCQRS\Abstracts\Command
 * @template TReturn
 */
interface CommandBus
{
    /**
     * @param  TCommand  $command
     * @return TReturn
     */
    public function send($command);
}
