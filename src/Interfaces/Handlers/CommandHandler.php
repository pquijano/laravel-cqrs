<?php

declare(strict_types=1);

namespace PQuijano\LaravelCQRS\Interfaces\Handlers;

/**
 * @template TCommand of \PQuijano\LaravelCQRS\Abstracts\Command
 * @template TReturn
 */
interface CommandHandler
{
    /**
     * @param  TCommand  $command
     * @return TReturn
     */
    public function handle($command);
}
