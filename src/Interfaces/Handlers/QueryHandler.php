<?php

declare(strict_types=1);

namespace PQuijano\LaravelCQRS\Interfaces\Handlers;

/**
 * @template TQuery of \PQuijano\LaravelCQRS\Abstracts\Query
 * @template TReturn
 */
interface QueryHandler
{
    /**
     * @param  TQuery  $query
     * @return TReturn
     */
    public function handle($query);
}
