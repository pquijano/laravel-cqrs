<?php

declare(strict_types=1);

namespace PQuijano\LaravelCQRS\Facades;

use Illuminate\Support\Facades\Facade as AbstractFacade;

final class CommandBus extends AbstractFacade
{
    protected static function getFacadeAccessor()
    {
        return \PQuijano\LaravelCQRS\Interfaces\Buses\CommandBus::class;
    }
}
