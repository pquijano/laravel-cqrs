<?php

declare(strict_types=1);

namespace PQuijano\LaravelCQRS\Abstracts;

use Spatie\LaravelData\Data;

abstract class Query extends Data
{
}
