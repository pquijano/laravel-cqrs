<?php

declare(strict_types=1);

namespace PQuijano\LaravelCQRS;

use Illuminate\Support\ServiceProvider as AbstractServiceProvider;

final class LaravelCQRSServiceProvider extends AbstractServiceProvider
{
    /**
     * All of the container bindings that should be registered.
     *
     * @var array
     */
    public $bindings = [
        Interfaces\Buses\CommandBus::class => Buses\CommandBus::class,
        Interfaces\Buses\QueryBus::class => Buses\QueryBus::class,
    ];
}
